import { Injectable, HttpService } from "@nestjs/common";
import * as sgMail from "@sendgrid/mail";
import * as functions from "firebase-functions";

@Injectable()
export class AppEmailService {
  private readonly config = functions.config().config;

  constructor(private httpService: HttpService) {
    sgMail.setApiKey(this.config.smtp);
  }

  public async sendEmail(body: any, ip: string) {
    // finish recaptcha
    const validate = await this.httpService
      .post(
        `https://www.google.com/recaptcha/api/siteverify?response=${body.recaptchaValue}&secret=${this.config.recaptcha}&remoteip=${ip}`
      )
      .toPromise();

    if (!validate.data.success) {
      console.log("Failed recaptcha:", validate.data);
      return new Error("Recaptcha failed.");
    }

    await sgMail.send({
      to: "rjpkuyper@gmail.com",
      from: "datails.smtp@gmail.com",
      subject: "Contact via de website",
      templateId: "d-d03335da99ef4d65b800a99582024ecc",
      dynamicTemplateData: {
        email: body.email,
        name: body.naam,
        body: body.message,
      },
    });
  }
}
