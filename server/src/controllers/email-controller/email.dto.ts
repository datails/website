import { IsNotEmpty, IsString, IsEmail } from "class-validator";

export class EmailDTO {
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @IsNotEmpty()
  @IsString()
  readonly naam: string;

  @IsNotEmpty()
  @IsString()
  readonly message: string;

  @IsNotEmpty()
  @IsString()
  readonly recaptchaValue: string;

  @IsNotEmpty()
  @IsString()
  readonly 'g-recaptcha-response': string
}
