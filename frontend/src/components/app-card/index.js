import React from "react";
import {
  makeStyles,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
// import Button from '../app-button';

const useStyles = makeStyles({
  card: {
    width: "100%",
    boxShadow: "0 0 2.5rem 0.3125rem rgba(0,0,0,.1)",
  },
  media: {
    height: "140px",
  },
  textDecorationNone: {
    textDecoration: "none!important",
  },
});

export default function AppCard({ buttonText, desc, image, title, href }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardActionArea src href={href} className={classes.textDecorationNone}>
        <CardMedia
          component="img"
          alt={title}
          height="140"
          image={image}
          title={title}
          className={classes.media}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {desc}
          </Typography>
        </CardContent>
        {href && buttonText ? (
          <CardActions>
            <Button href={href} size="small" color="primary">
              {buttonText}
            </Button>
          </CardActions>
        ) : (
          ""
        )}
      </CardActionArea>
    </Card>
  );
}
